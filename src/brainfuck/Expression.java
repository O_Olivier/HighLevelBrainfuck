package brainfuck;

import common.Tab;

public interface Expression{ Tab eval(Tab t); }
