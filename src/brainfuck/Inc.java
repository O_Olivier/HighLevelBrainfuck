package brainfuck;

import common.Tab;

public class Inc implements Expression{
    @Override public Tab eval(Tab t){return t.inc();}
}
