package brainfuck;

import common.Tab;

public class MoveLeft implements Expression{
    @Override public Tab eval(Tab t){return t.dec_pointer();}
}
