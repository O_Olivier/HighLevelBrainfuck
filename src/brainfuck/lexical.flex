package common;

%%

%public
%class Lexer
%type Token
%unicode

%eofval{
	return new Token(Sym.EOF);
%eofval}

%%
">"	{return new Token(Sym.RIGHT);}
"<" 	{return new Token(Sym.LEFT);}
"\+" 	{return new Token(Sym.INC);}
"\-" 	{return new Token(Sym.DEC);}
"." 	{return new Token(Sym.OUT);}
"," 	{return new Token(Sym.IN);}
"[" 	{return new Token(Sym.LBCKT);}
"]"	{return new Token(Sym.RBCKT);}
[^] 	{}