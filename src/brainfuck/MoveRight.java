package brainfuck;

import common.Tab;

public class MoveRight implements Expression{
    @Override public Tab eval(Tab t){return t.inc_pointer();}
}
