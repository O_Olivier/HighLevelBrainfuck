package brainfuck;

import common.Tab;

public class Out implements Expression{
    @Override public Tab eval(Tab t){
	System.out.print(t.get());
	return t;
    }
}
