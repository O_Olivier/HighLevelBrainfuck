package brainfuck;

import java.util.ArrayList;

import common.Tab;

public class Loop
    extends ArrayList<Expression>
    implements Expression{
    
    @Override
    public Tab eval(Tab t){
	if(t.get() == 0){
	    return t;
	}else{
	    this.forEach(exp -> exp.eval(t));
	    return eval(t);
	}
    }	
}
