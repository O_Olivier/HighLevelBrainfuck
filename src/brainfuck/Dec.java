package brainfuck;

import common.Tab;

public class Dec implements Expression{
    @Override public Tab eval(Tab t){return t.dec();}
}
