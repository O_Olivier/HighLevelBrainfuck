package brainfuck;

import java.util.ArrayList;

import common.Sym;
import common.LookAheadReader;

public class Parser{
    protected Tab t;
    protected LookAheadReader reader;
    protected ArrayList<Expression> list;

    public Parser(LookAheadReader r){
	this.t = new Tab();
	this.list = new ArrayList<>();
	this.reader = r;
    }

    public ArrayList<Expression> parse()
	throws Exception{
	while(!reader.check(Sym.EOF)){
	    list.add(parseOneExpression());
	}
	return list;

    }

    public Expression parseOneExpression()
	throws java.io.IOException, Exception{
	Expression e;
	if(reader.check(Sym.RIGHT)){
	    reader.eat(Sym.RIGHT);
	    e = new MoveRight();
	}else if(reader.check(Sym.LEFT)){
	    reader.eat(Sym.LEFT);
	    e = new MoveLeft();
	}else if(reader.check(Sym.INC)){
	    reader.eat(Sym.INC);
	    e = new Inc();
	}else if(reader.check(Sym.DEC)){
	    reader.eat(Sym.DEC);
	    e = new Dec();
	}else if(reader.check(Sym.OUT)){
	    reader.eat(Sym.OUT);
	    e = new Out();
	}else if(reader.check(Sym.IN)){
	    reader.eat(Sym.IN);
	    e = new In();
	}else if(reader.check(Sym.LBCKT)){
	    reader.eat(Sym.LBCKT);
	    e = loop();
	}else{
	    System.out.println(reader.getSymbol());
	    throw new Exception("Dude c'mon !");
	}
	return e;
    }

    public Loop loop()
	throws java.io.IOException, Exception{
	Loop l = new Loop();
	while(!reader.check(Sym.RBCKT)){
	    l.add(parseOneExpression());
	}
	reader.eat(Sym.RBCKT);
	return l;
    }
}
