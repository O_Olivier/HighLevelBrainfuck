package brainfuck;

import java.util.ArrayList;

import common.Tab;

public class Program extends ArrayList<Expression>{
    public Program(ArrayList<Expression> l){
	this.addAll(l);
    }

    public void execute(){
	Tab t = new Tab();
	this.forEach(e -> e.eval(t));
    }
}
