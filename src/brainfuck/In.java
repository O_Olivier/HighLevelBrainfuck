package brainfuck;

import java.util.Scanner;

import common.Tab;

public class In implements Expression{
    @Override public Tab eval(Tab t){
	return t.set((char)((new Scanner(System.in)).nextInt()));
    }
}
