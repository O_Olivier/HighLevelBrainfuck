package hlbf;

import brainfuck.Expression;
import common.Tab;

import java.util.ArrayList;

//CF wikipedia brainfuck : macros

abstract class HigherLevelExpression
    extends ArrayList<Expression>
    implements Expression {

    @Override
    public Tab eval(Tab t){
	this.forEach(e -> e.eval(t));
	return t;
    }
}


