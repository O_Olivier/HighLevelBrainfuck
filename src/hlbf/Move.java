package hlbf;

import brainfuck.*;

public class Move extends HigherLevelExpression{
    public Move(int s, int d) {
	this.add(new To(s));
	Loop l = new Loop();
	l.add(new Dec());
	l.add(new To(d));
	l.add(new Inc());
	l.add(new To(s));
	this.add(l);
    }
}
