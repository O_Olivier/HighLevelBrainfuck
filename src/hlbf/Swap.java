package hlbf;

class Swap extends HigherLevelExpression{
    public Swap(int a, int b, int t){
	this.add(new Move(a, t));
	this.add(new Move(b, a));
	this.add(new Move(t, b));
    }
}
