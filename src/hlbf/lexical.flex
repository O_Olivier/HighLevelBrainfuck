package common;


%%

%public
%class Lexer
%type Token
%unicode
%column
%line

%{
	public int getLine(){return yyline;}
	public int getColumn(){return yycolumn;}
%}

%eofval{
	return new Token(Sym.EOF);
%eofval}

%%
">"	{return new Token(Sym.RIGHT);}
"<" 	{return new Token(Sym.LEFT);}
"\+" 	{return new Token(Sym.INC);}
"\-" 	{return new Token(Sym.DEC);}
"." 	{return new Token(Sym.OUT);}
"," 	{return new Token(Sym.IN);}
"[" 	{return new Token(Sym.LBCKT);}
"]"	{return new Token(Sym.RBCKT);}
"to"	{return new Token(Sym.TO);}
"addCst"	{return new Token(Sym.ADDCST);}
"zero"		{return new Token(Sym.ZERO);}
"move"		{return new Token(Sym.MOVE);}
"move2"		{return new Token(Sym.MOVE2);}
"copy"		{return new Token(Sym.COPY);}
"if"		{return new Token(Sym.IF);}
"endif"		{return new Token(Sym.ENDIF);}
"ifelse"	{return new Token(Sym.IFELSE);}
"else"		{return new Token(Sym.ELSE);}
"endelse"	{return new Token(Sym.ENDELSE);}
"for"		{return new Token(Sym.FOR);}
"next"		{return new Token(Sym.NEXT);}
"swap"		{return new Token(Sym.SWAP);}
"brain"		{return new Token(Sym.BRAIN);}
"fuck"		{return new Token(Sym.FUCK);}
[0-9]+		{return new IntToken(Integer.parseInt(yytext())); }
[a-zA-Z][a-zA-Z0-9]*	{return new StringToken(yytext());}
[^] 	{}