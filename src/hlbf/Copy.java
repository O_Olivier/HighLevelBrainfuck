package hlbf;

public class Copy extends HigherLevelExpression{
    public Copy(int s, int d, int t){
	this.add(new Move2(s, d, t));
	this.add(new Move(t, s));
    }
}
