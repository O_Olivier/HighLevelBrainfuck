package hlbf;

import java.util.List;

import brainfuck.Expression;
import brainfuck.Loop;

class If extends HigherLevelExpression{
    public If(int a, List<Expression> exp){
	this.add(new To(a));
	Loop l = new Loop();
	l.addAll(exp);
	l.add(new Zero(a));
	this.add(l);
    }
}
