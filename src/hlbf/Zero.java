package hlbf;

import brainfuck.Loop;
import brainfuck.Dec;

public class Zero extends HigherLevelExpression{
    public Zero(int a){
	this.add(new To(a));
	Loop l = new Loop();
	l.add(new Dec());
	this.add(l);
    }
}
