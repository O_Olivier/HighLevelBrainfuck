package hlbf;

import common.Tab;

public class VarDeclare extends HigherLevelExpression{
    protected String varName;
    
    public VarDeclare(String varName) { this.varName = varName; }

    @Override
    public Tab eval(Tab t){
	t.declareVariable(varName);
	return t;
    }
}
class VarDeclareAndAssign extends VarDeclare{
    private char value;
    public VarDeclareAndAssign(String varName, char value){
	super(varName);
	this.value = value;
    }

    @Override
    public Tab eval(Tab t){
	t.declareVariable(varName);
	t.assign(varName, value);
	return t;
    }
}
