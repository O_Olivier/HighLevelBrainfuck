package hlbf;

import common.Tab;
import brainfuck.Expression;

public class AddCst implements Expression{
    private int n;
    public AddCst(int n) { this.n = n; }

    @Override
    public Tab eval(Tab t){
	while(n > 0) { t.inc(); n --;}
	while(n < 0) { t.dec(); n ++;}
	return t;
    }
    
}
