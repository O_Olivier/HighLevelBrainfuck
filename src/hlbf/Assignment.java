package hlbf;

import common.Tab;

public class Assignment extends HigherLevelExpression{
    private String varName;
    private char value;

    public Assignment(String varName, char value){
	this.varName = varName;
	this.value = value;
    }
    
    @Override
    public Tab eval(Tab t){
	t.assign(varName, value);
	return t;
    }
}
