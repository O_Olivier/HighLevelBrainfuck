package hlbf;

import brainfuck.*;

public class Move2 extends HigherLevelExpression{
    public Move2(int s, int d1, int d2){
	this.add(new To(s));
	Loop l = new Loop();
	l.add(new Dec());
	l.add(new To(d1));
	l.add(new Inc());
	l.add(new To(d2));
	l.add(new Inc());
	l.add(new To(s));
	this.add(l);
    }
}
