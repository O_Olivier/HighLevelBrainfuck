package hlbf;

import java.util.ArrayList;

import brainfuck.*;

import common.Sym;
import common.LookAheadReader;

public class Parser extends brainfuck.Parser{
    
    public Parser(LookAheadReader r){
	super(r);
    }

    public Expression parseOneExpression()
	throws java.io.IOException, Exception{
	Expression e;
	if(reader.check(Sym.RIGHT)){
	    reader.eat(Sym.RIGHT);
	    e = new MoveRight();
	}else if(reader.check(Sym.LEFT)){
	    reader.eat(Sym.LEFT);
	    e = new MoveLeft();
	}else if(reader.check(Sym.INC)){
	    reader.eat(Sym.INC);
	    e = new Inc();
	}else if(reader.check(Sym.DEC)){
	    reader.eat(Sym.DEC);
	    e = new Dec();
	}else if(reader.check(Sym.OUT)){
	    reader.eat(Sym.OUT);
	    e = new Out();
	}else if(reader.check(Sym.IN)){
	    reader.eat(Sym.IN);
	    e = new In();
	}else if(reader.check(Sym.LBCKT)){
	    reader.eat(Sym.LBCKT);
	    e = loop();
	}else if(reader.check(Sym.TO)){
	    reader.eat(Sym.TO);
	    e = to();
	}else if(reader.check(Sym.ADDCST)){
	    reader.eat(Sym.ADDCST);
	    e = addcst();
	}else if(reader.check(Sym.ZERO)){
	    reader.eat(Sym.ZERO);
	    e = zero();
	}else if(reader.check(Sym.MOVE)){
	    reader.eat(Sym.MOVE);
	    e = move();
	}else if(reader.check(Sym.MOVE2)){
	    reader.eat(Sym.MOVE2);
	    e = move2();
	}else if(reader.check(Sym.COPY)){
	    reader.eat(Sym.COPY);
	    e = copy();
	}else if(reader.check(Sym.IF)){
	    reader.eat(Sym.IF);
	    e = ifexp();
	}else if(reader.check(Sym.IFELSE)){
	    reader.eat(Sym.IFELSE);
	    e = ifalt();
	}else if(reader.check(Sym.FOR)){
	    reader.eat(Sym.FOR);
	    e = forexp();
	}else if(reader.check(Sym.SWAP)){
	    reader.eat(Sym.SWAP);
	    e = swap();
	}else if(reader.check(Sym.BRAIN)){
	    reader.eat(Sym.BRAIN);
	    e = varDeclar();
	}else if(reader.check(Sym.WORD)){
	    e = assignment();
	}else{
	    System.out.println(reader.getSymbol());
	    throw new Exception("Dude c'mon !");
	}
	return e;
    }

    public To to()
	throws Exception{
	int n = reader.getNum();
	reader.eat(Sym.NUM);
	return new To(n);
    }

    public AddCst addcst()
	throws Exception{
	int n = reader.getNum();
	reader.eat(Sym.NUM);
	return new AddCst(n);
    }

    public Zero zero()
	throws Exception{
	int n = reader.getNum();
	reader.eat(Sym.NUM);
	return new Zero(n);
    }

    public Move move()
	throws Exception{
	int s = reader.getNum();
	reader.eat(Sym.NUM);
	int d = reader.getNum();
	reader.eat(Sym.NUM);
	return new Move(s, d);
    }

    public Move2 move2()
	throws Exception{
	int s = reader.getNum();
	reader.eat(Sym.NUM);
	int d1 = reader.getNum();
	reader.eat(Sym.NUM);
	int d2 = reader.getNum();
	reader.eat(Sym.NUM);
	return new Move2(s, d1, d2);
    }

    public Copy copy()
	throws Exception{
	int s = reader.getNum();
	reader.eat(Sym.NUM);
	int d = reader.getNum();
	reader.eat(Sym.NUM);
	int t = reader.getNum();
	reader.eat(Sym.NUM);
	return new Copy(s, d, t);
    }

    public If ifexp()
	throws Exception{
	int a = reader.getNum();
	reader.eat(Sym.NUM);
	ArrayList<Expression> exp = new ArrayList<>();
	while(!reader.check(Sym.ENDIF)){
	    exp.add(parseOneExpression());
	}
	reader.eat(Sym.ENDIF);
	return new If(a, exp);
    }

    public IfAlt ifalt()
	throws Exception{
	int a = reader.getNum();
	reader.eat(Sym.NUM);
	int t = reader.getNum();
	reader.eat(Sym.NUM);
	ArrayList<Expression> exp = new ArrayList<>();
	while(!reader.check(Sym.ELSE)){
	    exp.add(parseOneExpression());
	}
	reader.eat(Sym.ELSE);
	ArrayList<Expression> exp2 = new ArrayList<>();
	while(!reader.check(Sym.ENDELSE)){
	    exp2.add(parseOneExpression());
	}
	reader.eat(Sym.ENDELSE);
	return new IfAlt(a, t, exp, exp2);
    }

    public For forexp()
	throws Exception{
	int s = reader.getNum();
	reader.eat(Sym.NUM);
	ArrayList<Expression> exp = new ArrayList<>();
	while(!reader.check(Sym.NEXT)){
	    exp.add(parseOneExpression());
	}
	reader.eat(Sym.NEXT);
	return new For(s, exp);
    }

    public Swap swap()
	throws Exception{
	int a = reader.getNum();
	reader.eat(Sym.NUM);
	int b = reader.getNum();
	reader.eat(Sym.NUM);
	int t = reader.getNum();
	reader.eat(Sym.NUM);
	return new Swap(a, b, t);
    }

    public VarDeclare varDeclar()
	throws Exception{
	String varName = reader.getWord();
	reader.eat(Sym.WORD);
	if(reader.check(Sym.FUCK)){
	    reader.eat(Sym.FUCK);
	    char value = (char)reader.getNum();
	    reader.eat(Sym.NUM);
	    return new VarDeclareAndAssign(varName,value);
	}
	return new VarDeclare(varName);
    }

    public Assignment assignment()
	throws Exception{
	String varName = reader.getWord();
	reader.eat(Sym.WORD);
	reader.eat(Sym.FUCK);
	char value = (char)reader.getNum();
	reader.eat(Sym.NUM);
	return new Assignment(varName, value);
    }
}
