package hlbf;

import common.Tab;
import brainfuck.Expression;

public class To implements Expression{
    private int a;
    public To(int a) { this.a = a; }
    
    @Override
    public Tab eval (Tab t){
	while(t.getPointer() < a) { t.inc_pointer(); }
	while(t.getPointer() > a) { t.dec_pointer(); }
	return t;
    }
}
