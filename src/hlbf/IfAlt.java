package hlbf;

import java.util.List;

import brainfuck.Expression;
import brainfuck.Loop;
import brainfuck.Inc;
import brainfuck.Dec;

class IfAlt extends HigherLevelExpression{
    public IfAlt(int a, int t, List<Expression> exp, List<Expression> exp2){
	this.add(new Zero(t));
	this.add(new To(t));
	this.add(new Inc());

	//IF
	exp.add(new Zero(t));
	this.add(new If(a, exp));

	//ELSE
	this.add(new If(t, exp2));
    }
}
