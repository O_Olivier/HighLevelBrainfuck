package hlbf;

import java.util.List;

import brainfuck.Expression;
import brainfuck.Loop;
import brainfuck.Dec;

class For extends HigherLevelExpression{
    public For(int s, List<Expression> exp){
	this.add(new To(s));
	Loop l = new Loop();
	l.addAll(exp);
	l.add(new To(s));
	l.add(new Dec());
	this.add(l);
    }
}
