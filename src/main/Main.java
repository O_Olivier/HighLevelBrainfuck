package main;

import java.io.*;

import common.Lexer;
import common.LookAheadReader;
import hlbf.Parser;
import hlbf.Program;

class Main{
    public static void main(String[] args) throws Exception{
	if(args.length > 0){
	    Reader reader = new FileReader(new File(args[0]));
	    Lexer lexer = new Lexer(reader);
	    LookAheadReader look = new LookAheadReader(lexer);
	    Parser parser = new Parser(look);
	    Program prog = new Program(parser.parse());
	    prog.execute();
	}
    }
}
