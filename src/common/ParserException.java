package common;

public class ParserException extends Exception{
    
    public ParserException(int line, int column){
        super("Error while parsing line "+line
            +" column "+column +". "
        );
    }
}
