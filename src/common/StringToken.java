package common;

public class StringToken extends Token{
    String word;
    
    public StringToken(String word){
	super(Sym.WORD);
	this.word = word;
    }

    public String getWord() { return word; }
}
