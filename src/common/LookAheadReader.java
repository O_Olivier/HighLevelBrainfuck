package common;

public class LookAheadReader{
    private Lexer lexer;
    private Token current;

    public LookAheadReader(Lexer lexer)
	throws Exception{
	this.lexer = lexer;
	this.current = lexer.yylex();
    }

    public boolean check(Sym s) { return current.symbol() == s; }

    public void eat(Sym s)
	throws Exception{
	if(check(s))
	    current = lexer.yylex();
	else throw new ParserException(lexer.getLine(), lexer.getColumn());
    }

    public Sym getSymbol(){ return current.symbol(); }	

    public int getNum() { return ((IntToken)current).getNum(); }

    public String getWord() { return ((StringToken)current).getWord(); }
}
