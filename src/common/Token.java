package common;

public class Token{
    Sym s;
    
    public Token(Sym s){this.s = s;}

    public Sym symbol(){return s;}
}
