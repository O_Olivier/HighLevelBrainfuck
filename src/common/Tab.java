package common;

import java.util.HashMap;

public class Tab{
    
    private int pointer;
    private char[] t;
    private HashMap<String,Integer> variables;

    public Tab(){
	pointer = 0;
	t = new char[30_000];
	variables = new HashMap<>();
    }
    
    public Tab inc_pointer() { pointer++; return this; }
    public Tab dec_pointer() { pointer--; return this; }
    public Tab inc() { t[pointer] ++;return this; }
    public Tab dec() { t[pointer] --;return this; }
    
    public char get(){return t[pointer];}
    public int getPointer(){return pointer;}
    public Tab set(char c){t[pointer] = c;return this;}

    public void declareVariable(String name){
	variables.put(name, nextUnallocatedSpace());
	pointer ++;
    }

    public int nextUnallocatedSpace(){
	pointer ++;
	return pointer;
    }

    public void assign(String varName, char value){
	if(variables.containsKey(varName)){
	    t[variables.get(varName)] = value;
	}
    }
}
