package common;

public class IntToken extends Token{
    private int n;
    public IntToken(int n) { super(Sym.NUM); this.n = n; }
    public int getNum() { return this.n ; }
}
